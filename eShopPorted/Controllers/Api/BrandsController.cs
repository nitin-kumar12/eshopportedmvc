﻿using eShopPorted.Services;
using Microsoft.AspNetCore.Mvc;

namespace eShopPorted.Controllers.Api
{
    [ApiController]
    [Route("api/[controller]")]
    public class BrandsController : Controller
    {
        private ICatalogService _service;

        public BrandsController(ICatalogService service)
        {
            _service = service;
        }

        // GET api/<controller>
        //public IEnumerable<Models.CatalogBrand> Get()
        //{
        //    var brands = _service.GetCatalogBrands();
        //    return brands;
        //}
        [Route("Index")]
        public IActionResult Index()
        {
            var brands = _service.GetCatalogBrands();
            return Ok(brands);
        }

        // GET api/<controller>/5
        public IActionResult Get(int id)
        {
            var brands = _service.GetCatalogBrands();
            var brand = brands.FirstOrDefault(x => x.Id == id);
            if (brand == null) return NotFound();

            return Ok(brand);
        }

        [HttpDelete]
        // DELETE api/<controller>/5
        public IActionResult Delete(int id)
        {
            var brandToDelete = _service.GetCatalogBrands().FirstOrDefault(x => x.Id == id);
            if (brandToDelete == null)
            {
                return NotFound();
            }

            // demo only - don't actually delete
            return Ok();
        }
    }
}