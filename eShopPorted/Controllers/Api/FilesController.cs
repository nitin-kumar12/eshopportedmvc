﻿using eShopPorted.Services;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace eShopPorted.Controllers.Api
{
    [ApiController]
    [Route("api/[controller]")]
    public class FilesController : Controller
    {
        private ICatalogService _service;

        public FilesController(ICatalogService service)
        {
            _service = service;
        }

        // GET api/<controller>
        //public HttpResponseMessage Get()
        //{
        //    var brands = _service.GetCatalogBrands()
        //        .Select(b => new BrandDTO
        //        {
        //            Id = b.Id,
        //            Brand = b.Brand
        //        }).ToList();
        //    var serializer = new Serializing();
        //    var response = new HttpResponseMessage(HttpStatusCode.OK)
        //    {
        //        Content = new StreamContent(serializer.SerializeBinary(brands))
        //    };

        //    return response;
        //}

        public IActionResult Index()
        {
            var brands = _service.GetCatalogBrands()
                .Select(b => new BrandDTO
                {
                    Id = b.Id,
                    Brand = b.Brand
                }).ToList();

            var data = JsonSerializer.Serialize(brands);

            return Ok(data);
        }

        [Serializable]
        public class BrandDTO
        {
            public int Id { get; set; }
            public string Brand { get; set; }
        }
    }
}