﻿using eShopPorted.Models.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace eShopPorted.Models
{
    public class CatalogDBContext : DbContext
    {
        public CatalogDBContext(DbContextOptions<CatalogDBContext> options) : base(options)
        {

        }

        public DbSet<CatalogItem> CatalogItems { get; set; }

        public DbSet<CatalogBrand> CatalogBrands { get; set; }

        public DbSet<CatalogType> CatalogTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //ConfigureCatalogType(builder.Entity<CatalogType>());
            //ConfigureCatalogBrand(builder.Entity<CatalogBrand>());
            //ConfigureCatalogItem(builder.Entity<CatalogItem>());
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }

        //void ConfigureCatalogType(EntityTypeBuilder<CatalogType> builder)
        //{
        //    builder.ToTable("CatalogType");

        //    builder.HasKey(ci => ci.Id);

        //    builder.Property(ci => ci.Id)
        //       .IsRequired();

        //    builder.Property(cb => cb.Type)
        //        .IsRequired()
        //        .HasMaxLength(100);
        //    builder.HasData(
        //        new CatalogType { Id = 1, Type = "Mug" },
        //        new CatalogType { Id = 2, Type = "T-Shirt" },
        //        new CatalogType { Id = 3, Type = "Sheet" },
        //        new CatalogType { Id = 4, Type = "USB Memory Stick" }
        //    );
        //}

        //void ConfigureCatalogBrand(EntityTypeBuilder<CatalogBrand> builder)
        //{
        //    builder.ToTable("CatalogBrand");

        //    builder.HasKey(ci => ci.Id);

        //    builder.Property(ci => ci.Id)
        //       .IsRequired();

        //    builder.Property(cb => cb.Brand)
        //        .IsRequired()
        //        .HasMaxLength(100);
        //}

        //void ConfigureCatalogItem(EntityTypeBuilder<CatalogItem> builder)
        //{
        //    builder.ToTable("Catalog");

        //    builder.HasKey(ci => ci.Id);

        //    builder.Property(ci => ci.Id)
        //        .ValueGeneratedNever()
        //        .IsRequired();

        //    builder.Property(ci => ci.Name)
        //        .IsRequired()
        //        .HasMaxLength(50);

        //    builder.Property(ci => ci.Price)
        //        .IsRequired();

        //    builder.Property(ci => ci.PictureFileName)
        //        .IsRequired();

        //    builder.Ignore(ci => ci.PictureUri);

        //    builder.HasOne<CatalogBrand>(ci => ci.CatalogBrand)
        //        .WithMany()
        //        .HasForeignKey(ci => ci.CatalogBrandId);

        //    builder.HasOne<CatalogType>(ci => ci.CatalogType)
        //        .WithMany()
        //        .HasForeignKey(ci => ci.CatalogTypeId);
        //}
    }
}
